#!/usr/bin/env python3

# Copyright 2019 James Smith

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import time
import threading

import boto3

__doc__ = """
Re-encrypt the EBS Volumes attached to an EC2 instance with a new KMS key.
The instance is stopped to ensure a consistent snapshot of the disks.
"""

def printer(s, indent=0):
    print('    '*indent, s)

def is_powered_on(session, instance_id):
    """Find the power state of the instance"""
    ec2 = session.resource('ec2')
    instance = ec2.Instance(instance_id)
    return instance.state

def restore_power(session, instance_id, initial_power_state):
    """Restart the instance if initial_power_state was running"""
    ec2 = session.resource('ec2')
    instance = ec2.Instance(instance_id)

    if initial_power_state['Code'] == 16:
        printer(f'Instance {instance.id} was initially powered on. Restarting...')
        instance.start()
        instance.wait_until_running()
        printer(f'Instance started.')

def stop_instance(session, instance_id):
    """Stop the EC2 instance with id"""
    ec2 = session.resource('ec2')
    instance = ec2.Instance(instance_id)
    printer(f"Stopping Instance {instance.id}")
    instance.stop()
    printer(f"Waiting for Instance to stop...", 1)
    instance.wait_until_stopped()
    printer(f"Instance {instance.id} stopped", 1)


def get_volumes(session, instance_id):
    """Gets a list of volumes attached to the instance id"""
    ec2 = session.resource('ec2')
    volume_iterator = ec2.volumes.filter(
        Filters=[
            {
                'Name': 'attachment.instance-id',
                'Values': [
                    instance_id
                ]
            }
        ]
    )
    return [volume.id for volume in volume_iterator]


def snapshot_volume(session, volume_id):
    """Create a snapshot of the volume id"""
    ec2 = session.resource('ec2')
    volume = ec2.Volume(volume_id)

    start_time = time.asctime()
    printer(f"Snapshotting {volume_id} at {start_time}", 1)
    snapshot = volume.create_snapshot(
        Description=f'KMS key change for {volume_id}'
    )

    printer(f"Waiting for snapshot to complete...", 2)
    snapshot.wait_until_completed()

    end_time = time.asctime()
    printer(f"Snapshot {snapshot.id} complete at {end_time}", 2)
    return (volume_id, snapshot.id)


def change_kms_key(session, snapshot_id, region, key_arn=None):
    """Change the KMS key of the snapshot by copying it"""
    ec2 = session.resource('ec2')
    snapshot = ec2.Snapshot(snapshot_id)

    printer(f"Copying snapshot {snapshot_id}...", 1)
    if key_arn:
        encrypted_snapshot = snapshot.copy(
            Description=f'Copy of snapshot {snapshot_id} with new key',
            Encrypted=True,
            SourceRegion=region,
            KmsKeyId=key_arn)
    else:
        encrypted_snapshot = snapshot.copy(
            Description=f'Copy of snapshot {snapshot_id} with new key',
            Encrypted=True,
            SourceRegion=region)

    copied_snapshot = ec2.Snapshot(encrypted_snapshot['SnapshotId'])
    printer(f"Created new encrypted snapshot {copied_snapshot.id}", 2)
    printer(f"Waiting for the copy to complete...", 2)
    copied_snapshot.wait_until_completed()
    printer(f"Copy of snapshot {copied_snapshot.id} complete.",2 )
    return (snapshot_id, copied_snapshot.id)


def create_new_volume(session, old_volume, snapshot_id):
    """Create a new volume using the re-encrypted snapshot"""
    ec2 = session.resource('ec2')
    # Get meta data about the old volume

    volume = ec2.Volume(old_volume)
    printer('Creating new volume from snapshot {snapshot_id}...', 1)
    # Apend a new tag to the volume describing the original volume ID
    if volume.tags is None:
        volume.create_tags(
            Tags=[
                {
                    'Key': 'cleardata:copied_from',
                    'Value': old_volume
                }
            ]
        )
    else:
        volume.tags.append({'Key': 'cleardata:copied_from', 'Value': old_volume})

    old_volume_tags = [
        {
            'ResourceType': 'volume',
            'Tags': volume.tags
        }
    ]
    old_volume_az = volume.availability_zone
    old_volume_type = volume.volume_type
    volume = ec2.create_volume(
        AvailabilityZone=old_volume_az,
        SnapshotId=snapshot_id,
        VolumeType=old_volume_type,
        TagSpecifications=old_volume_tags)
    
    printer(f'Created new volume {volume.id} from snapshot {snapshot_id}', 2)

    # The Volume resource doesn't create a waiter for us, so we have to create one manually
    printer(f'Waiting for volume {volume.id} to become available', 2)
    ec2_client = session.client('ec2')
    waiter = ec2_client.get_waiter('volume_available')
    waiter.wait(
        VolumeIds=[
            volume.id
        ]
    )
    printer(f'Created new volume {volume.id}', 2)
    return volume.id

def replace_volumes(session, instance_id, old_volume_id, new_volume_id):
    ec2 = session.resource('ec2')
    old_volume = ec2.Volume(old_volume_id)
    new_volume = ec2.Volume(new_volume_id)

    printer(f'Replacing volume {old_volume_id} with {new_volume_id}', 1)
    for attachment in old_volume.attachments:
        device_name = attachment['Device']
        printer(f'Detaching old volume {old_volume.id} from instance {instance_id} on device {device_name}', 2)
        old_volume.detach_from_instance(
            Device=device_name,
            InstanceId=instance_id
        )

        ec2_client = session.client('ec2')
        waiter = ec2_client.get_waiter('volume_available')
        waiter.wait(
            VolumeIds=[
                old_volume.id
            ]
        )
        printer(f'Detached volume {old_volume.id}', 2)

        new_volume.attach_to_instance(
            Device=device_name,
            InstanceId=instance_id
        )

        waiter = ec2_client.get_waiter('volume_in_use')
        waiter.wait(
            VolumeIds=[
                new_volume.id
            ]
        )
        printer(f'Attached volume {new_volume.id}', 2)

def reencrypt_volumes_on_instance(session, instance_id, region, key_arn):
    """Re-encrypt each volume attached to an instance"""
    initial_power_state = is_powered_on(session, instance_id)
    stop_instance(session, instance_id)
    volumes = get_volumes(session, instance_id)

    # Get all volumes attached to the instance

    threads = []
    # Create a snapshot of each volume
    for volume_id in volumes:
        ## Create a thread for each volume for the snapshot step, as it takes the longest.
        thread = threading.Thread(target=snapshot_volume, args=(session, volume_id))
        thread.start()
        threads.append(thread)
    
    ## Re-join all threads and complete the rest of the steps single-threaded.
    for thread in threads:
        thread.join()    
    
    for volume_id in volumes:
        # Copy each snapshot to a new snapshot with the new KMS key
        (_, copied_snapshot_id) = change_kms_key(session, snapshot_id, region, key_arn)

        # Create a new volume from the snapshot, using the
        # attributes from the original volume
        new_volume_id = create_new_volume(session, volume_id, copied_snapshot_id)

        # Detach old volumes and attach new volumes
        replace_volumes(session, instance_id, volume_id, new_volume_id)
    
    restore_power(session, instance_id, initial_power_state)
    printer('Done.')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("profile", help="AWS configuration profile to authenticate with")
    parser.add_argument("region", help="AWS Region")
    parser.add_argument("instanceID", help="EC2 Instance ID")
    parser.add_argument("--KmsKeyId", help="KMS key id or alias ARN")
    parser.parse_args()

    args = parser.parse_args()

    # Create login session
    session = boto3.Session(profile_name=args.profile, region_name=args.region)
    reencrypt_volumes_on_instance(session, args.instanceID, args.region, args.KmsKeyId)


if __name__ == "__main__":
    main()
