#

## Usage

You can install the dependencies via pip like so:

`python3 -m venv .`  
`source bin/activate`  
`pip3 install -r requirements.txt`  
`python3 main.py --help`  